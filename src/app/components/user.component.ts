import { Component } from '@angular/core';
import { PostsService } from '../services/posts.service'

@Component({
	moduleId: module.id,
  	selector: 'user',
  	templateUrl: 'user.component.html',
  	providers: [PostsService]
})

export class UserComponent  {
	name: string;
	email: string;
	address: address;
	hobbies: string[];
	showHobbies: boolean;
	posts: string[];

	constructor(private postsService: PostsService){
		this.name = 'John Smith';
		this.email = 'jsmith@goo.com';
		this.address = {
			street: '12 Main Street',
			city: 'London',
			state: 'MA'
		};
		this.hobbies = ['Music', "Sweeming", 'Photography'];
		this.showHobbies = false;

		this.postsService.getPost().subscribe(posts => {
			this.posts = posts;
		});
	}

	toggleHobbies(){
		if(this.showHobbies){
			this.showHobbies = false;
		}else{
			this.showHobbies = true;
		}
	}

	addHobby(hobby: string){
		this.hobbies.push(hobby);
	}

	deleteHobby(i:number){
		this.hobbies.splice(i, 1);	
	}

}

interface address{
	street: string;
	city: string;
	state: string;
}

interface Post{
	id: number;
	title: string;
	body: string;
}